# StrawberryFields Rails

- [3.0.0]
- [2.9.0] - 2024-08-10 - 11 files changed, 175 insertions(+), 96 deletions(-)
- [2.8.0] - 2024-03-10 - 5 files changed, 86 insertions(+), 22 deletions(-)
- [2.7.0] - 2024-02-04 - 9 files changed, 67 insertions(+), 29 deletions(-)
- [2.6.0] - 2023-11-11 - 6 files changed, 30 insertions(+), 21 deletions(-)
- [2.5.0] - 2023-11-05 - 20 files changed, 353 insertions(+), 210 deletions(-)
- [2.4.0] - 2023-04-16 - 11 files changed, 174 insertions(+), 126 deletions(-)
- [2.3.0] - 2022-11-27 - 20 files changed, 238 insertions(+), 11 deletions(-)
- [2.2.0] - 2022-11-20 - 7 files changed, 11 insertions(+), 8 deletions(-)
- [2.1.0] - 2022-06-26 - 6 files changed, 13 insertions(+), 23 deletions(-)
- [2.0.0] - 2022-03-13 - 9 files changed, 242 insertions(+), 69 deletions(-)
- [1.3.0] - 2021-10-11 - 6 files changed, 47 insertions(+), 4 deletions(-)
- [1.2.0] - 2021-10-01 - 4 files changed, 9 insertions(+), 2 deletions(-)
- [1.1.0] - 2021-09-25 - 6 files changed, 14 insertions(+), 10 deletions(-)
- 1.0.0 - 2021-04-29

[3.0.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/344f5c39...master
[2.9.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/773f20d0...344f5c39
[2.8.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/2b1f6b3a...773f20d0
[2.7.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/2c05de14...2b1f6b3a
[2.6.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/190810d8...2c05de14
[2.5.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/b5072546...190810d8
[2.4.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/828283dd...b5072546
[2.3.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/d7efb83d...828283dd
[2.2.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/0b7cab8f...d7efb83d
[2.1.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/4a2febb3...0b7cab8f
[2.0.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/bfb16d8b...4a2febb3
[1.3.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/ea84dd39...bfb16d8b
[1.2.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/05e20f87...ea84dd39
[1.1.0]: https://gitlab.com/acefed/strawberryfields-rails/-/compare/b9334213...05e20f87
