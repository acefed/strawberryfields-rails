Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Render dynamic PWA files from app/views/pwa/*
  get "service-worker" => "rails/pwa#service_worker", as: :pwa_service_worker
  get "manifest" => "rails/pwa#manifest", as: :pwa_manifest

  # Defines the root path route ("/")
  # root "posts#index"

  root "home#index"

  get "/about", to: "about#show"

  get "/u/:username", to: "u_user#show"

  get "/u/:username/inbox", to: "inbox#show"
  post "/u/:username/inbox", to: "inbox#create"

  post "/u/:username/outbox", to: "outbox#create"
  get "/u/:username/outbox", to: "outbox#show"

  get "/u/:username/following", to: "following#show"

  get "/u/:username/followers", to: "followers#show"

  post "/s/:secret/u/:username", to: "s_send#create"

  get "/.well-known/nodeinfo", to: "nodeinfo#show"

  get "/.well-known/webfinger", to: "webfinger#show"

  get "/@", to: redirect("/", status: 302)
  get "/u", to: redirect("/", status: 302)
  get "/user", to: redirect("/", status: 302)
  get "/users", to: redirect("/", status: 302)
  get "/users/:username", to: redirect("/u/%{username}", status: 302)
  get "/user/:username", to: redirect("/u/%{username}", status: 302)
  get "/@:username", to: redirect("/u/%{username}", status: 302)
end
