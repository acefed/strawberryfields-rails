class WebfingerController < ApplicationController
  layout false

  def show
    username = CONFIG["actor"][0]["preferredUsername"]
    hostname = URI.parse(CONFIG["origin"]).host
    p443 = "https://#{hostname}:443/"
    resource = params[:resource]
    has_resource = false
    resource = "https://#{hostname}/#{resource[p443.length..-1]}" if resource.start_with?(p443)
    has_resource = true if resource == "acct:#{username}@#{hostname}"
    has_resource = true if resource == "mailto:#{username}@#{hostname}"
    has_resource = true if resource == "https://#{hostname}/@#{username}"
    has_resource = true if resource == "https://#{hostname}/u/#{username}"
    has_resource = true if resource == "https://#{hostname}/user/#{username}"
    has_resource = true if resource == "https://#{hostname}/users/#{username}"
    return render file: "public/404.html", status: 404 if !has_resource
    body = {
      subject: "acct:#{username}@#{hostname}",
      aliases: [
        "mailto:#{username}@#{hostname}",
        "https://#{hostname}/@#{username}",
        "https://#{hostname}/u/#{username}",
        "https://#{hostname}/user/#{username}",
        "https://#{hostname}/users/#{username}"
      ],
      links: [
        {
          rel: "self",
          type: "application/activity+json",
          href: "https://#{hostname}/u/#{username}"
        },
        {
          rel: "http://webfinger.net/rel/avatar",
          type: "image/png",
          href: "https://#{hostname}/public/#{username}u.png"
        },
        {
          rel: "http://webfinger.net/rel/profile-page",
          type: "text/plain",
          href: "https://#{hostname}/u/#{username}"
        }
      ]
    }
    headers = {
      "Cache-Control": "public, max-age=#{CONFIG['ttl']}, must-revalidate",
      Vary: "Accept, Accept-Encoding"
    }
    render json: body, content_type: "application/jrd+json", headers: headers
  end
end
