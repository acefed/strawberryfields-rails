class OutboxController < ApplicationController
  layout false
  skip_forgery_protection

  def create
    render file: "public/405.html", status: 405
  end

  def show
    username = params[:username]
    hostname = URI.parse(CONFIG["origin"]).host
    return render file: "public/404.html", status: 404 if username != CONFIG["actor"][0]["preferredUsername"]
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/outbox",
      type: "OrderedCollection",
      totalItems: 0
    }
    render json: body, content_type: "application/activity+json"
  end
end
