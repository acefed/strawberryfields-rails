require "json"
require "uri"

class InboxController < ApplicationController
  layout false
  skip_forgery_protection

  def show
    render file: "public/405.html", status: 405
  end

  def create
    username = params[:username]
    hostname = URI.parse(CONFIG["origin"]).host
    content_type_header_field = request.env["CONTENT_TYPE"]
    has_type = false
    y = JSON.parse(request.body.read)
    t = y["type"] || ""
    aid = y["id"] || ""
    atype = y["type"] || ""
    return render file: "public/400.html", status: 400 if aid.length > 1024 or atype.length > 64
    puts "INBOX #{aid} #{atype}"
    return render file: "public/404.html", status: 404 if username != CONFIG["actor"][0]["preferredUsername"]
    has_type = true if content_type_header_field.include?("application/activity+json")
    has_type = true if content_type_header_field.include?("application/ld+json")
    has_type = true if content_type_header_field.include?("application/json")
    return render file: "public/400.html", status: 400 if !has_type
    return render file: "public/400.html", status: 400 if !request.headers["HTTP_DIGEST"] or !request.headers["HTTP_SIGNATURE"]
    return render plain: "", status: 200 if t == "Accept" or t == "Reject" or t == "Add"
    return render plain: "", status: 200 if t == "Remove" or t == "Like" or t == "Announce"
    return render plain: "", status: 200 if t == "Create" or t == "Update" or t == "Delete"
    if t == "Follow"
      return render file: "public/400.html", status: 400 if URI.parse(y["actor"] || "").scheme != "https"
      x = get_activity username, hostname, y["actor"]
      return render file: "public/500.html", status: 500 if !x
      accept_follow username, hostname, x, y
      return render plain: "", status: 200
    end
    if t == "Undo"
      z = y["object"] || {}
      t = z["type"] || ""
      return render plain: "", status: 200 if t == "Accept" or t == "Like" or t == "Announce"
      if t == "Follow"
        return render file: "public/400.html", status: 400 if URI.parse(y["actor"] || "").scheme != "https"
        x = get_activity username, hostname, y["actor"]
        return render file: "public/500.html", status: 500 if !x
        accept_follow username, hostname, x, z
        return render plain: "", status: 200
      end
    end
    render file: "public/500.html", status: 500
  end
end
