ActiveSupport.escape_html_entities_in_json = false

class UUserController < ApplicationController
  layout false

  def show
    public_key_pem = PRIVATE_KEY.public_key.to_s

    username = params[:username]
    hostname = URI.parse(CONFIG["origin"]).host
    accept_header_field = request.env["HTTP_ACCEPT"]
    has_type = false
    return render file: "public/404.html", status: 404 if username != CONFIG["actor"][0]["preferredUsername"]
    has_type = true if accept_header_field.include?("application/activity+json")
    has_type = true if accept_header_field.include?("application/ld+json")
    has_type = true if accept_header_field.include?("application/json")
    if !has_type then
      body = "#{username}: #{CONFIG['actor'][0]['name']}" 
      headers = {
        "Cache-Control": "public, max-age=#{CONFIG['ttl']}, must-revalidate",
        Vary: "Accept, Accept-Encoding"
      }
      return render plain: body, headers: headers
    end
    body = {
      "@context": [
        "https://www.w3.org/ns/activitystreams",
        "https://w3id.org/security/v1",
        {
          schema: "https://schema.org/",
          PropertyValue: "schema:PropertyValue",
          value: "schema:value",
          Key: "sec:Key"
        }
      ],
      id: "https://#{hostname}/u/#{username}",
      type: "Person",
      inbox: "https://#{hostname}/u/#{username}/inbox",
      outbox: "https://#{hostname}/u/#{username}/outbox",
      following: "https://#{hostname}/u/#{username}/following",
      followers: "https://#{hostname}/u/#{username}/followers",
      preferredUsername: username,
      name: CONFIG["actor"][0]["name"],
      summary: "<p>3.0.0</p>",
      url: "https://#{hostname}/u/#{username}",
      endpoints: {sharedInbox: "https://#{hostname}/u/#{username}/inbox"},
      attachment: [
        {
          type: "PropertyValue",
          name: "me",
          value: ME
        }
      ],
      icon: {
        type: "Image",
        mediaType: "image/png",
        url: "https://#{hostname}/public/#{username}u.png"
      },
      image: {
        type: "Image",
        mediaType: "image/png",
        url: "https://#{hostname}/public/#{username}s.png"
      },
      publicKey: {
        id: "https://#{hostname}/u/#{username}#Key",
        type: "Key",
        owner: "https://#{hostname}/u/#{username}",
        publicKeyPem: public_key_pem
      }
    }
    headers = {
      "Cache-Control": "public, max-age=#{CONFIG['ttl']}, must-revalidate",
      Vary: "Accept, Accept-Encoding"
    }
    render json: body, content_type: "application/activity+json", headers: headers
  end
end
