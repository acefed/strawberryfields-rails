require "json"
require "openssl"
require "time"
require "uri"
require "http"

CONFIG =
  if ENV["CONFIG_JSON"] then
    config_json = ENV["CONFIG_JSON"]
    config_json = config_json[1..-1] if config_json.start_with?("'")
    config_json = config_json[0..-2] if config_json.end_with?("'")
    JSON.parse(config_json)
  else
    File.open("data/config.json", "r") do |f|
      JSON.parse(f.read)
    end
  end
ME = [
  '<a href="https://',
  URI.parse(CONFIG["actor"][0]["me"]).host,
  '/" rel="me nofollow noopener noreferrer" target="_blank">',
  "https://",
  URI.parse(CONFIG["actor"][0]["me"]).host,
  "/",
  "</a>"
].join()
private_key_pem = ENV["PRIVATE_KEY"]
private_key_pem = private_key_pem.split("\\n").join("\n")
private_key_pem = private_key_pem[1..-1] if private_key_pem.start_with?('"')
private_key_pem = private_key_pem[0..-2] if private_key_pem.end_with?('"')
PRIVATE_KEY = OpenSSL::PKey::RSA.new(private_key_pem)

# controllers
# |-- about_controller.rb
# |-- application_controller.rb
# |-- followers_controller.rb
# |-- following_controller.rb
# |-- home_controller.rb
# |-- inbox_controller.rb
# |-- nodeinfo_controller.rb
# |-- outbox_controller.rb
# |-- s_send_controller.rb
# |-- u_user_controller.rb
# `-- webfinger_controller.rb

class ApplicationController < ActionController::Base
  # Only allow modern browsers supporting webp images, web push, badges, import maps, CSS nesting, and CSS :has.
  allow_browser versions: :modern

  def uuidv7
    v = SecureRandom.random_bytes(16).bytes
    ts = (Time.now.to_f * 1000).to_i
    v[0] = (ts >> 40) & 0xFF
    v[1] = (ts >> 32) & 0xFF
    v[2] = (ts >> 24) & 0xFF
    v[3] = (ts >> 16) & 0xFF
    v[4] = (ts >> 8) & 0xFF
    v[5] = ts & 0xFF
    v[6] = (v[6] & 0x0F) | 0x70
    v[8] = (v[8] & 0x3F) | 0x80
    v.pack('C*').unpack1('H*')
  end

  def talk_script(req)
    ts = (Time.now.to_f * 1000).to_i
    return "<p>#{ts}</p>" if URI.parse(req).host == "localhost"
    [
      "<p>",
      '<a href="https://',
      URI.parse(req).host,
      '/" rel="nofollow noopener noreferrer" target="_blank">',
      URI.parse(req).host,
      "</a>",
      "</p>"
    ].join()
  end

  def get_activity(username, hostname, req)
    t = Time.now.utc.httpdate
    sig = PRIVATE_KEY.sign(OpenSSL::Digest::SHA256.new, [
      "(request-target): get #{URI.parse(req).path}",
      "host: #{URI.parse(req).host}",
      "date: #{t}"
    ].join("\n"))
    b64 = Base64.strict_encode64(sig)
    headers = {
      Date: t,
      Signature: [
        "keyId=\"https://#{hostname}/u/#{username}#Key\"",
        'algorithm="rsa-sha256"',
        'headers="(request-target) host date"',
        "signature=\"#{b64}\""
      ].join(","),
      Accept: "application/activity+json",
      "Accept-Encoding": "identity",
      "Cache-Control": "no-cache",
      "User-Agent": "StrawberryFields-Rails/3.0.0 (+https://#{hostname}/)"
    }
    res = HTTP[headers].get(req)
    status = res.code
    puts "GET #{req} #{status}"
    JSON.parse(res)
  end

  def post_activity(username, hostname, req, x)
    t = Time.now.utc.httpdate
    body = x.to_json
    s256 = Digest::SHA256.base64digest(body)
    sig = PRIVATE_KEY.sign(OpenSSL::Digest::SHA256.new, [
      "(request-target): post #{URI.parse(req).path}",
      "host: #{URI.parse(req).host}",
      "date: #{t}",
      "digest: SHA-256=#{s256}"
    ].join("\n"))
    b64 = Base64.strict_encode64(sig)
    headers = {
      Date: t,
      Digest: "SHA-256=#{s256}",
      Signature: [
        "keyId=\"https://#{hostname}/u/#{username}#Key\"",
        'algorithm="rsa-sha256"',
        'headers="(request-target) host date digest"',
        "signature=\"#{b64}\""
      ].join(","),
      Accept: "application/json",
      "Accept-Encoding": "gzip",
      "Cache-Control": "max-age=0",
      "Content-Type": "application/activity+json",
      "User-Agent": "StrawberryFields-Rails/3.0.0 (+https://#{hostname}/)"
    }
    puts "POST #{req} #{body}"
    HTTP[headers].post(req, body: body)
    nil
  end

  def accept_follow(username, hostname, x, y)
    aid = uuidv7
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/s/#{aid}",
      type: "Accept",
      actor: "https://#{hostname}/u/#{username}",
      object: y
    }
    post_activity username, hostname, x["inbox"], body
  end

  def follow(username, hostname, x)
    aid = uuidv7
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/s/#{aid}",
      type: "Follow",
      actor: "https://#{hostname}/u/#{username}",
      object: x["id"]
    }
    post_activity username, hostname, x["inbox"], body
  end

  def undo_follow(username, hostname, x)
    aid = uuidv7
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/s/#{aid}#Undo",
      type: "Undo",
      actor: "https://#{hostname}/u/#{username}",
      object: {
        id: "https://#{hostname}/u/#{username}/s/#{aid}",
        type: "Follow",
        actor: "https://#{hostname}/u/#{username}",
        object: x["id"]
      }
    }
    post_activity username, hostname, x["inbox"], body
  end

  def like(username, hostname, x, y)
    aid = uuidv7
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/s/#{aid}",
      type: "Like",
      actor: "https://#{hostname}/u/#{username}",
      object: x["id"]
    }
    post_activity username, hostname, y["inbox"], body
  end

  def undo_like(username, hostname, x, y)
    aid = uuidv7
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/s/#{aid}#Undo",
      type: "Undo",
      actor: "https://#{hostname}/u/#{username}",
      object: {
        id: "https://#{hostname}/u/#{username}/s/#{aid}",
        type: "Like",
        actor: "https://#{hostname}/u/#{username}",
        object: x["id"]
      }
    }
    post_activity username, hostname, y["inbox"], body
  end

  def announce(username, hostname, x, y)
    aid = uuidv7
    t = Time.now.utc.iso8601
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/s/#{aid}/activity",
      type: "Announce",
      actor: "https://#{hostname}/u/#{username}",
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"],
      object: x["id"]
    }
    post_activity username, hostname, y["inbox"], body
  end
  
  def undo_announce(username, hostname, x, y, z)
    aid = uuidv7
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/s/#{aid}#Undo",
      type: "Undo",
      actor: "https://#{hostname}/u/#{username}",
      object: {
        id: "#{z}/activity",
        type: "Announce",
        actor: "https://#{hostname}/u/#{username}",
        to: ["https://www.w3.org/ns/activitystreams#Public"],
        cc: ["https://#{hostname}/u/#{username}/followers"],
        object: x["id"]
      }
    }
    post_activity username, hostname, y["inbox"], body
  end
  
  def create_note(username, hostname, x, y)
    aid = uuidv7
    t = Time.now.utc.iso8601
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/s/#{aid}/activity",
      type: "Create",
      actor: "https://#{hostname}/u/#{username}",
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"],
      object: {
        id: "https://#{hostname}/u/#{username}/s/#{aid}",
        type: "Note",
        attributedTo: "https://#{hostname}/u/#{username}",
        content: talk_script(y),
        url: "https://#{hostname}/u/#{username}/s/#{aid}",
        published: t,
        to: ["https://www.w3.org/ns/activitystreams#Public"],
        cc: ["https://#{hostname}/u/#{username}/followers"]
      }
    }
    post_activity username, hostname, x["inbox"], body
  end
  
  def create_note_image(username, hostname, x, y, z)
    aid = uuidv7
    t = Time.now.utc.iso8601
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/s/#{aid}/activity",
      type: "Create",
      actor: "https://#{hostname}/u/#{username}",
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"],
      object: {
        id: "https://#{hostname}/u/#{username}/s/#{aid}",
        type: "Note",
        attributedTo: "https://#{hostname}/u/#{username}",
        content: talk_script("https://localhost"),
        url: "https://#{hostname}/u/#{username}/s/#{aid}",
        published: t,
        to: ["https://www.w3.org/ns/activitystreams#Public"],
        cc: ["https://#{hostname}/u/#{username}/followers"],
        attachment: [
          {
            type: "Image",
            mediaType: z,
            url: y
          }
        ]
      }
    }
    post_activity username, hostname, x["inbox"], body
  end
  
  def create_note_mention(username, hostname, x, y, z)
    aid = uuidv7
    t = Time.now.utc.iso8601
    at = "@#{y['preferredUsername']}@#{URI.parse(y['inbox']).host}"
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/s/#{aid}/activity",
      type: "Create",
      actor: "https://#{hostname}/u/#{username}",
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"],
      object: {
        id: "https://#{hostname}/u/#{username}/s/#{aid}",
        type: "Note",
        attributedTo: "https://#{hostname}/u/#{username}",
        inReplyTo: x["id"],
        content: talk_script(z),
        url: "https://#{hostname}/u/#{username}/s/#{aid}",
        published: t,
        to: ["https://www.w3.org/ns/activitystreams#Public"],
        cc: ["https://#{hostname}/u/#{username}/followers"],
        tag: [
          {
            type: "Mention",
            name: at
          }
        ]
      }
    }
    post_activity username, hostname, y["inbox"], body
  end
  
  def create_note_hashtag(username, hostname, x, y, z)
    aid = uuidv7
    t = Time.now.utc.iso8601
    body = {
      "@context": [
        "https://www.w3.org/ns/activitystreams",
        {"Hashtag": "as:Hashtag"},
      ],
      id: "https://#{hostname}/u/#{username}/s/#{aid}/activity",
      type: "Create",
      actor: "https://#{hostname}/u/#{username}",
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"],
      object: {
        id: "https://#{hostname}/u/#{username}/s/#{aid}",
        type: "Note",
        attributedTo: "https://#{hostname}/u/#{username}",
        content: talk_script(y),
        url: "https://#{hostname}/u/#{username}/s/#{aid}",
        published: t,
        to: ["https://www.w3.org/ns/activitystreams#Public"],
        cc: ["https://#{hostname}/u/#{username}/followers"],
        tag: [
          {
            type: "Hashtag",
            name: "##{z}"
          }
        ]
      }
    }
    post_activity username, hostname, x["inbox"], body
  end
  
  def create_note(username, hostname, x, y)
    aid = uuidv7
    t = Time.now.utc.iso8601
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/s/#{aid}/activity",
      type: "Create",
      actor: "https://#{hostname}/u/#{username}",
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"],
      object: {
        id: "https://#{hostname}/u/#{username}/s/#{aid}",
        type: "Note",
        attributedTo: "https://#{hostname}/u/#{username}",
        content: talk_script(y),
        url: "https://#{hostname}/u/#{username}/s/#{aid}",
        published: t,
        to: ["https://www.w3.org/ns/activitystreams#Public"],
        cc: ["https://#{hostname}/u/#{username}/followers"]
      }
    }
    post_activity username, hostname, x["inbox"], body
  end

  def update_note(username, hostname, x, y)
    t = Time.now.utc.iso8601
    ts = (Time.now.to_f * 1000).to_i
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "#{y}##{ts}",
      type: "Update",
      actor: "https://#{hostname}/u/#{username}",
      published: t,
      to: ["https://www.w3.org/ns/activitystreams#Public"],
      cc: ["https://#{hostname}/u/#{username}/followers"],
      object: {
        id: y,
        type: "Note",
        attributedTo: "https://#{hostname}/u/#{username}",
        content: talk_script("https://localhost"),
        url: y,
        updated: t,
        to: ["https://www.w3.org/ns/activitystreams#Public"],
        cc: ["https://#{hostname}/u/#{username}/followers"]
      }
    }
    post_activity username, hostname, x["inbox"], body
  end

  def delete_tombstone(username, hostname, x, y)
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "#{y}#Delete",
      type: "Delete",
      actor: "https://#{hostname}/u/#{username}",
      object: {
        id: y,
        type: "Tombstone"
      }
    }
    post_activity username, hostname, x["inbox"], body
  end
end
