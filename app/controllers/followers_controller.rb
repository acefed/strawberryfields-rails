class FollowersController < ApplicationController
  layout false

  def show
    username = params[:username]
    hostname = URI.parse(CONFIG["origin"]).host
    return render file: "public/404.html", status: 404 if username != CONFIG["actor"][0]["preferredUsername"]
    body = {
      "@context": "https://www.w3.org/ns/activitystreams",
      id: "https://#{hostname}/u/#{username}/followers",
      type: "OrderedCollection",
      totalItems: 0
    }
    render json: body, content_type: "application/activity+json"
  end
end
