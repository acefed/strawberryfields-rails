class NodeinfoController < ApplicationController
  def show
    hostname = URI.parse(CONFIG["origin"]).host
    body = {
      links: [
        {
          rel: "http://nodeinfo.diaspora.software/ns/schema/2.0",
          href: "https://#{hostname}/nodeinfo/2.0.json"
        },
        {
          rel: "http://nodeinfo.diaspora.software/ns/schema/2.1",
          href: "https://#{hostname}/nodeinfo/2.1.json"
        }
      ]
    }
    headers = {
      "Cache-Control": "public, max-age=#{CONFIG['ttl']}, must-revalidate",
      Vary: "Accept, Accept-Encoding"
    }
    render json: body, headers: headers
  end
end
