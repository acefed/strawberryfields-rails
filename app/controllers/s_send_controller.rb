require "uri"

class SSendController < ApplicationController
  layout false
  skip_forgery_protection

  def create
    username = params[:username]
    hostname = URI.parse(CONFIG["origin"]).host
    send = JSON.parse(request.body.read)
    t = send["type"] || ""
    return render file: "public/404.html", status: 404 if username != CONFIG["actor"][0]["preferredUsername"]
    return render file: "public/404.html", status: 404 if !params[:secret] or params[:secret] == "-"
    return render file: "public/404.html", status: 404 if params[:secret] != ENV["SECRET"]
    return render file: "public/400.html", status: 400 if URI.parse(send["id"] || "").scheme != "https"
    x = get_activity username, hostname, send["id"]
    return render file: "public/500.html", status: 500 if !x
    aid = x["id"] || ""
    atype = x["type"] || ""
    return render file: "public/400.html", status: 400 if aid.length > 1024 or atype.length > 64
    if t == "follow"
      follow username, hostname, x
      return render plain: "", status: 200
    end
    if t == "undo_follow"
      undo_follow username, hostname, x
      return render plain: "", status: 200
    end
    if t == "like"
      return render file: "public/400.html", status: 400 if URI.parse(x["attributedTo"] || "").scheme != "https"
      y = get_activity username, hostname, x["attributedTo"]
      return render file: "public/500.html", status: 500 if !y
      like username, hostname, x, y
      return render plain: "", status: 200
    end
    if t == "undo_like"
      return render file: "public/400.html", status: 400 if URI.parse(x["attributedTo"] || "").scheme != "https"
      y = get_activity username, hostname, x["attributedTo"]
      return render file: "public/500.html", status: 500 if !y
      undo_like username, hostname, x, y
      return render plain: "", status: 200
    end
    if t == "announce"
      return render file: "public/400.html", status: 400 if URI.parse(x["attributedTo"] || "").scheme != "https"
      y = get_activity username, hostname, x["attributedTo"]
      return render file: "public/500.html", status: 500 if !y
      announce username, hostname, x, y
      return render plain: "", status: 200
    end
    if t == "undo_announce"
      return render file: "public/400.html", status: 400 if URI.parse(x["attributedTo"] || "").scheme != "https"
      y = get_activity username, hostname, x["attributedTo"]
      return render file: "public/500.html", status: 500 if !y
      if send["url"] && !send["url"].empty?
        z = send["url"]
      else
        z = "https://#{hostname}/u/#{username}/s/00000000000000000000000000000000"
      end
      return 400 if URI.parse(z).scheme != "https"
      undo_announce username, hostname, x, y, z
      return render plain: "", status: 200
    end
    if t == "create_note"
      if send["url"] && !send["url"].empty?
        y = send["url"]
      else
        y = "https://localhost"
      end
      return render file: "public/400.html", status: 400 if URI.parse(y).scheme != "https"
      create_note username, hostname, x, y
      return render plain: "", status: 200
    end
    if t == "create_note_image"
      if send["url"] && !send["url"].empty?
        y = send["url"]
      else
        y = "https://#{hostname}/public/logo.png"
      end
      return render file: "public/400.html", status: 400 if URI.parse(y).scheme != "https"
      return render file: "public/400.html", status: 400 if URI.parse(y).host != hostname
      z =
        if y.end_with?(".jpg") or y.end_with?(".jpeg")
          "image/jpeg"
        elsif y.end_with?(".svg")
          "image/svg+xml"
        elsif y.end_with?(".gif")
          "image/gif"
        elsif y.end_with?(".webp")
          "image/webp"
        elsif y.end_with?(".avif")
          "image/avif"
        else
          "image/png"
        end
      create_note_image username, hostname, x, y, z
      return render plain: "", status: 200
    end
    if t == "create_note_mention"
      return render file: "public/400.html", status: 400 if URI.parse(x["attributedTo"] || "").scheme != "https"
      y = get_activity username, hostname, x["attributedTo"]
      return render file: "public/500.html", status: 500 if !y
      if send["url"] && !send["url"].empty?
        z = send["url"]
      else
        z = "https://localhost"
      end
      return render file: "public/400.html", status: 400 if URI.parse(z).scheme != "https"
      create_note_mention username, hostname, x, y, z
      return render plain: "", status: 200
    end
    if t == "create_note_hashtag"
      if send["url"] && !send["url"].empty?
        y = send["url"]
      else
        y = "https://localhost"
      end
      return render file: "public/400.html", status: 400 if URI.parse(y).scheme != "https"
      if send["tag"] && !send["tag"].empty?
        z = send["tag"]
      else
        z = "Hashtag"
      end
      create_note_hashtag username, hostname, x, y, z
      return render plain: "", status: 200
    end
    if t == "update_note"
      if send["url"] && !send["url"].empty?
        y = send["url"]
      else
        y = "https://#{hostname}/u/#{username}/s/00000000000000000000000000000000"
      end
      return render file: "public/400.html", status: 400 if URI.parse(y).scheme != "https"
      update_note username, hostname, x, y
      return render plain: "", status: 200
    end
    if t == "delete_tombstone"
      if send["url"] && !send["url"].empty?
        y = send["url"]
      else
        y = "https://#{hostname}/u/#{username}/s/00000000000000000000000000000000"
      end
      return render file: "public/400.html", status: 400 if URI.parse(y).scheme != "https"
      delete_tombstone username, hostname, x, y
      return render plain: "", status: 200
    end
    puts "TYPE #{aid} #{atype}"
    render plain: "", status: 200
  end
end
