#!/usr/bin/env bash

set -e

if [ ! -f .env.example ]; then
  curl -sLO https://gitlab.com/acefed/strawberryfields-rails/-/raw/master/.env.example
fi
if [ ! -f data/config.json.example ]; then
  curl -sLo data/config.json.example --create-dirs https://gitlab.com/acefed/strawberryfields-rails/-/raw/master/data/config.json.example
fi

if [ "$1" = 'https://example' ] || [ "$1" = 'https://www.example.com' ]; then
  echo 'This is an example.'
elif [ $# -gt 0 ]; then
  sed -e "s|https://example|$1|g" data/config.json.example > data/config.json
fi

if docker images | grep -q strawberryfields-rails; then
  echo "SECRET_KEY_BASE=$(docker run --rm --entrypoint=secret strawberryfields-rails)" > .env
else
  echo "SECRET_KEY_BASE=$(rails secret)" > .env
fi
echo "$(tr -dc a-zA-Z0-9 </dev/urandom 2>/dev/null | head -c48)" >> secret.txt
echo "SECRET=$(tail -1 secret.txt)" >> .env
if command -v openssl >/dev/null; then
  if [ ! -f id_rsa ]; then
    openssl genpkey -quiet -algorithm rsa -pkeyopt rsa_keygen_bits:4096 -out id_rsa
  fi
  if [ ! -f id_rsa.pub ]; then
    openssl rsa -pubout -in id_rsa -out id_rsa.pub 2>/dev/null
  fi
elif command -v ssh-keygen >/dev/null; then
  if [ ! -f id_rsa ]; then
    ssh-keygen -q -b 4096 -m PKCS8 -t rsa -N '' -f id_rsa
  fi
  if [ ! -f id_rsa.pub ]; then
    ssh-keygen -e -m PKCS8 -f id_rsa > id_rsa.pub
  fi
fi
echo "PRIVATE_KEY=\"$(sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' id_rsa)\"" >> .env
